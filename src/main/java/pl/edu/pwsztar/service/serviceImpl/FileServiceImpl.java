package pl.edu.pwsztar.service.serviceImpl;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.files.GenerateTxt;
import pl.edu.pwsztar.domain.repository.MovieRepository;
import java.util.*;
import org.springframework.core.io.InputStreamResource;
import pl.edu.pwsztar.domain.files.GenerateTxt;
import pl.edu.pwsztar.service.FileService;
import java.io.*;
    @Service
    public class FileServiceImpl implements GenerateTxt, FileService {
    private final MovieRepository movieRepository;
    private final Converter<List<Movie>, List<FileDto>> converter;

            public FileServiceImpl(MovieRepository movieRepository,
                           Converter<List<Movie>, List<FileDto>> converter) {
               this.movieRepository = movieRepository;
                this.converter = converter;
            }

            public List<FileDto> getMoviesSorted(){
                List<Movie> movies = movieRepository.findAll(Sort.by("year").descending());
                return converter.convert(movies);
            }
        @Override
            public InputStreamResource toTxt(List<FileDto> fileDto) throws IOException {
                File file=File.createTempFile("tmp", ".txt");
                FileOutputStream outputStream=new FileOutputStream(file);
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
                    for(FileDto movie : fileDto){
                        bufferedWriter.write(movie.getYear() + " " + movie.getTitle());
                        bufferedWriter.newLine();
                }
                    bufferedWriter.close();
                    outputStream.flush();
                    outputStream.close();

                    InputStream inputStream = new FileInputStream(file);
                   return new InputStreamResource(inputStream);
               }
    }

